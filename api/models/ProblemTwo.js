this.cases
this.result
const constructor = (arr) => {
    this.cases = []
    for (let index = 1; index < (arr[0] * 2); index += 2) {
        const data = {}
        let params = []
        params = arr[index].split(" ")
        data.length = params[0]
        data.addCost = params[1]
        data.copyCost = params[2]
        data.str = arr[index + 1].toLowerCase()
        this.cases.push(data)
    }
}
const solve_problem = () => {
    this.result = []
    this.cases.forEach(element => {
        const length = element.length
        let finalStr = ''
        let finalCost = 0
        let subStr = ''

        for (let i = 0; i < length; i++) {
            subStr = element.str[i]
            while (finalStr.includes(subStr)) {
                if (finalStr.includes(subStr + element.str[i + 1])) {
                    subStr += element.str[i + 1]
                    i++
                } else break
            }
            // console.log(subStr)
            finalStr += subStr
            if (subStr.length > 1) {
                if ((subStr.length * element.addCost) > element.copyCost) {
                    finalCost = finalCost + parseInt(element.copyCost )
                }
            } else finalCost = finalCost + parseInt(element.addCost)
        }
        this.result.push(finalCost)
    });
}
const get_result = () => {
    return this.result
}

module.exports = {
    constructor,
    solve_problem,
    get_result
}