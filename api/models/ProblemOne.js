const all_equal = (str) => {
    const strArr = str.split('')
    return strArr.every( (e) => e === strArr[0])
}

const get_max_when_equal = (str) => {
    const strLength = str.length
    let substr = ''
    let results = []

    for (let i = 0; i < strLength; i++) {
        let count = 0
        substr += str[i]
        for (let j = 0; j < str.length; j++) {
            let strCopy = str.substring(j)
            if (strCopy.includes(substr)) {
                count++
            }
        }
        results.push(count * substr.length)
    }

    return Math.max(...results)
}

module.exports = {
    all_equal,
    get_max_when_equal
}