const ProblemOne = require('../models/ProblemOne')
const ProblemTwo = require('../models/ProblemTwo')

const solve_one = (req, res) => {
    const str = req.body.toLowerCase()
    let result
    // check if all elems in string are the same
    if (ProblemOne.all_equal(str)) {
        result = ProblemOne.get_max_when_equal(str)
    } else {
        result = str.length
    }

    res.json({ result })
}

const solve_two = (req, res) => {
    // Split incomming lines into an array
    const test = req.body.split('\n')

    // Set the cases in the class
    ProblemTwo.constructor(test)

    // Solve the problem
    ProblemTwo.solve_problem()

    res.json({ result: ProblemTwo.get_result() })
}

module.exports = {
    solve_one,
    solve_two
}