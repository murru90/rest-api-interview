const express = require('express');
const problemController = require('../controllers/problemController');
const router = express.Router();

router.post('/problem-1', problemController.solve_one);
router.post('/problem-2', problemController.solve_two);

module.exports = router;