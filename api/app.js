const express = require('express')
const cors = require('cors')
const app = express()
const problemRoutes = require('./routes/problemRoutes');

// app.use(express.urlencoded({ extended: true }))
app.use(cors())

app.use(express.text());
app.use('/', problemRoutes);
/* app.get('/', (req, res) => {
  res.json([
    {
      "id":"1",
      "title":"Book Review: The Name of the Wind"
    },
    {
      "id":"2",
      "title":"Game Review: Pokemon Brillian Diamond"
    },
    {
      "id":"3",
      "title":"Show Review: Alice in Borderland"
    }
  ])
}) */

app.listen(4000)