# rest-api-interview



## Getting started

Follow these steps:

1-) Just clone or download the project.
2-) cd into the project root folder (you have to see the api directory and docker-compose.yaml file).
3-) Run docker-compose build (just for the first time).
4-) Run docker-compose up.
5-) Test with an API testing software, I used Insomnia ;).

## Endpoints
* localhost:4000/problem-1
* localhost:4000/problem-2

Both endpoints receive a POST request with plain text as format.

ENJOY!